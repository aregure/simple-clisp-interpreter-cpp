# simple-clisp-interpreter-cpp

A simple Common Lisp interpreter, implemented in C++.

## What is implemented?
- Basic arithmetic functions (addition, subtraction, multiplication, and division)
- `car` and `cdr` functions

## Usage:

1. Compile the source code with your compiler of choice
2. Run the executable, in which you will be placed in a REPL (read-eval-print loop). Type `exit` and press <kbd>Enter</kbd> or press <kbd>Ctrl</kbd> + <kbd>C</kbd> to exit.

## Sample

![Sample](sample.png)
