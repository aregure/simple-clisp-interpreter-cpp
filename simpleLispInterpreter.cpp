/*--------------------------------------
 *  A simple "Common Lisp" interpreter
 *--------------------------------------*/
 
#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>
#include <stack>
using namespace std;

// check if character is a number
inline bool isnum(char c) {
    return c>='0' && c<='9';
}

// check if string is alphanumeric
inline bool notAlNum(char c) {
    return !isalnum(c);
}
bool isAlphaNum(const string & str) {
    return find_if(str.begin(), str.end(), notAlNum) == str.end();
}

// class definition for Interpreter
class Interpreter {
	private:
		// expression is stored here
		stringstream expr;
		
		// token types
		enum tType {
			INT_LIT,		// integer literal
			IDENT,			// identifier
			FUNCTION,		// function (car, cdr)
			OPTR,			// operator
			LEFTP,			// left parenthesis
			RIGHTP,			// right parenthesis
			SQUOTE,			// single quote mark
			END				// EOF
		};
		
		// token struct (can hold either numbers or strings)
		struct Token {
			tType tokenType;
			double value;
			string data;
			Token (tType type, double val = 0, string dat = "") : tokenType(type), value(val), data(dat) {}
		};
		
		// main stack for tokens
		stack<Token> tStk;
		
		// ignore spaces
		void getNonBlank() {
			while (expr.peek() == ' ') expr.get();
		}
		
		// gets next token
		Token getNext() {
			getNonBlank();
			if (expr.peek() == EOF) return Token(END);
			
			else if (expr.peek() == '(') {
				// if left paren, advance to next character and return left paren token
				expr.get();
				return Token(LEFTP);
			} else if (expr.peek() == ')') {
				// if right paren, advance to next character and return right paren token
				expr.get();
				return Token(RIGHTP);
			} else if (expr.peek() == '\'') {
				// if single quote, advance to next character and return quote token
				expr.get();
				return Token(SQUOTE);
			} else if (string("+-*/").find(expr.peek()) != string::npos) {
				// check if character matches any operator
				char op = expr.get();
				string oper = "";
				
				// if there is no space after the operator, it might be a negative sign
				//     so not an operator
				if (expr.peek() != ' ') expr.unget();
				else {
					// otherwise, copy character to string and return operator token
					//     with operator as string value
					oper += op;
					return Token(OPTR, 0, oper);
				}
			} else if (isnum(expr.peek()) || expr.peek() == '-') {
				// make sure next token is numeric
				double num;
				// copy number from stringstream
				expr >> num;
				// if there is an invalid character after the number, token
				//     is invalid.
				if (expr.peek() != ' ' && expr.peek() != ')'){
					throw Error("Expected space or right paren.");
				}
				// otherwise, return integer literal token
				return Token(INT_LIT, num);
			}
			// string stores either function or identifier
			string word = "";
			// only get alphanumeric substring (e.g. ignore
			//     right paren if operand in car/cdr function
			char ch;
			while (isalnum(ch = expr.peek())) {
				word += ch;
				expr.get();
			}
			// if word is car or cdr, then it's a function
			if (word == "car" || word == "cdr") {
				return Token(FUNCTION, 0, word);
			} else if (isAlphaNum(word)) {
				// otherwise, it's an identifier
				return Token(IDENT, 0, word);
			}
			// any invalid tokens result in an error
			throw Error("Invalid token.");
		}
	
	public:
		// struct for throwing error messages
		struct Error {
			string error;
			Error(const string & err) : error(err) {}
		};
		
		// copy expression from user input to stringstream
		void setExp (const string & ex) {
			expr.write(ex.c_str(), ex.size());
		}
		
		// helper function to push results of sub-expressions to main stack
		void evaluate_aux(stack<Token> & stk) {
			// "operator" token
			Token op = stk.top();
			// pop "operator" from stack, since it's already copied
			stk.pop();
			// if it's an integer literal, just push back to main stack
			if (op.tokenType == INT_LIT) tStk.push(op);
			// check if token is not an operator or function
			if (op.tokenType != OPTR && op.tokenType != FUNCTION) {
				// if it's an identifier, push back to main stack
				if (op.tokenType == IDENT) {
					tStk.push(op);
					return;
				} else {
					// otherwise, throw error message
					throw Error("Expected operator or function");
				}
			} else {
				// apply operations based on the operator
				if (op.tokenType == OPTR) {
					double result = stk.top().value;
					// pop first operand from stack
					stk.pop();
					if (op.data == "+") {
						// add all remaining operands to first operand
						//     then push back to main stack
						while (!stk.empty()) {
							result += stk.top().value;
							stk.pop();
						}
						tStk.push(Token(INT_LIT, result, ""));
						return;
					} else if (op.data == "-") {
						// subtract all remaining operands to first operand
						//     then push back to main stack
						while (!stk.empty()) {
							result -= stk.top().value;
							stk.pop();
						}
						tStk.push(Token(INT_LIT, result, ""));
						return;
					} else if (op.data == "*") {
						// multiply all remaining operands to first operand
						//     then push back to main stack
						while (!stk.empty()) {
							result *= stk.top().value;
							stk.pop();
						}
						tStk.push(Token(INT_LIT, result, ""));
						return;
					} else if (op.data == "/") {
						// divide first operand by remaining operands
						//     then push back to main stack
						while (!stk.empty()) {
							result /= stk.top().value;
							stk.pop();
						}
						tStk.push(Token(INT_LIT, result, ""));
						return;
					}
				} else if (op.tokenType == FUNCTION) {
					if (op.data == "car") {
						// check if topmost is a quote
						if (stk.top().tokenType == SQUOTE) {
							// get rid of quote mark
							tStk.push(stk.top());
							stk.pop();
							// push first element to main stack
							tStk.push(stk.top());
							// empty temporary stack to avoid duplicates
							while (!stk.empty()) {
								stk.pop();
							}
							return;
						} else {
							// otherwise, assume single element
							tStk.push(stk.top());
							return;
						}
					} else if (op.data == "cdr") {
						// check if topmost is quote
						if (stk.top().tokenType == SQUOTE) {
							// pop quote to main stack
							tStk.push(stk.top());
							stk.pop();
							// pop first element
							stk.pop();
							while (!stk.empty()) {
								// pop to main stack
								tStk.push(stk.top());
								stk.pop();
							}
						} else {
							// if no quote, assume single element
							while (!stk.empty()) {
								tStk.push(stk.top());
								stk.pop();
							}
							return;
						}
					}
				}
			}
		}
		
		void pusher() {
			// initialize temporary stack
			stack<Token> temp;
			// loop until end of expr
			while (expr.peek() != EOF) {
				// get next token
				Token t = getNext();
				if (t.tokenType == IDENT) {
					throw Error("Expected left paren.");
				}
				// any non right paren shall be pushed
				while (t.tokenType != RIGHTP) {
					tStk.push(t);
					t = getNext();
				}
				// pop to temp stack until left paren
				while (tStk.top().tokenType != LEFTP) {
					temp.push(tStk.top());
					tStk.pop();
				}
				// get rid of left paren
				tStk.pop();
				// if top of stack is an int, then check if there is a quote
				if (temp.top().tokenType == INT_LIT) {
					// if there is a quote, pop back contents to main stack
					if (tStk.top().tokenType == SQUOTE) {
						while (!temp.empty()) {
							tStk.push(temp.top());
							temp.pop();
						}
					} else {
						// otherwise, push integer literals to main stack
						while (!temp.empty()) {
							tStk.push(temp.top());
							temp.pop();
						}
					}
					// prematurely end iteration to avoid erroneous evaluation
					continue;
				}
				// call helper function to evaluate sub-expression and push result
				//     to main stack
				evaluate_aux(temp);
			}
			
			// stack to store reverse order, to display output correctly
			stack<Token> rev;
			// copy original stack to other stack in reverese order
			while (!tStk.empty()) {
				if (tStk.top().tokenType == SQUOTE) {
					// ignore any stray quote marks after single
					//     car/cdr function
					tStk.pop();
				} else {
					rev.push(tStk.top());
					tStk.pop();
				}
			}
			// display output before doing repl again
			while (!rev.empty()) {
				cout << "[" << rev.top().data << " : " << rev.top().value << "] ";
				rev.pop();
			}
			cout << endl;
		}
};

int main() {
	cout << "-------------------------------------------\n";
	cout << "| I got a LISP so I pronounce /s/ as /sh/ |\n";
	cout << "|        (a.k.a. IGALSIP/S/A/SH/)         |\n";
	cout << "|   A simple \"Common Lisp\" interpreter    |\n";
	cout << "| By John Paul Alegre [18-020010] (2020)  |\n";
	cout << "-------------------------------------------\n\n";
	cout << "Press Ctrl + C or type \"exit\" and press Enter to exit.\n\n";
	
	// read-eval-print loop (repl)
	while (1) {
		// prompt user for expression
		cout << "[exp]: ";
		string exp;
		// get the expression
		getline (cin, exp);
		// instantiate interpreter
		Interpreter test;
		// copy expression to interpreter's stringstream
		test.setExp(exp);
		// if user did not enter anything, just re-prompt.
		if (exp.size() == 0)
			continue;
		// quit program if user types 'exit'
		else if (exp == "exit") return 0;
		// call pusher method to push tokens to stack.
		try {
			test.pusher();
		} catch (const Interpreter::Error & error) {
			// any errors will be printed here.
			cout << error.error << endl;
		}
	}
}
